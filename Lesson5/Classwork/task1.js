/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/
let Train = {
	name: 'Kiev',
	speed: 140,
	passangers: 80,
	go: function () {
		console.log('Поезд '+ this.name + " везет "+ this.passangers + " cо скоростью "+ this.speed );
	},
	stop: function () {
		this.speed = 0;
		console.log('Поезд '+ this.name + " остановился. "+ "Скорость "+ this.speed );
	},
	getPassangers: function (number) {
		this.passangers+= number;
		console.log('Количество новых пассажиров '+ this.passangers );
	}

};
Train.go();
Train.stop();
Train.getPassangers(20);