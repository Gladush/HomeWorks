/*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.

    Например:
      encryptCesar('Word', 3);
      encryptCesar1(...)
      ...
      encryptCesar5(...)

      decryptCesar1('Sdwq', 3);
      decryptCesar1(...)
      ...
      decryptCesar5(...)

*/ 

        var alphabet = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
        function shiftAlphabet(shift) {
            console.log(shift)
            var shiftedAlphabet = ''; //новый алфавит 
            for (var i = 0; i < alphabet.length; i++) {
                // console.log(i, alphabet[i+shift]);
                currentLetter = (alphabet[i + shift] === undefined) ? (alphabet[i + shift - alphabet.length]) : (alphabet[i + shift]); //Текущая буква со сдвигом, если выходим за рамки длины алфавита - берем с начала алфавита
 
                shiftedAlphabet = shiftedAlphabet.concat(currentLetter);
            }
            return shiftedAlphabet;
        }
 
        // console.log(shiftedAlphabet);
 
        function encrypt() {
            var message = document.getElementById('message').value;
            var shift = parseInt(document.getElementById('shift').value);
            var shiftedAlphabet = shiftAlphabet(shift);
            var encryptedMessage = '';
            for (var i = 0; i < message.length; i++) {
                var indexOfLetter = alphabet.indexOf(message[i].toUpperCase());
                encryptedMessage = encryptedMessage.concat(shiftedAlphabet[indexOfLetter]);
            }
            document.getElementById('cipher').value = encryptedMessage.toLowerCase();
        }
 
        function decrypt() {
            var message = document.getElementById('cipher').value;
            var shift = parseInt(document.getElementById('shift').value);
            var shiftedAlphabet = shiftAlphabet(shift);
            var encryptedMessage = '';
            for (var i = 0; i < message.length; i++) {
                if (message[i] == ' ') {
                    encryptedMessage = encryptedMessage.concat(' ');
                    continue};
                var indexOfLetter = shiftedAlphabet.indexOf(message[i].toUpperCase());
                encryptedMessage = encryptedMessage.concat(alphabet[indexOfLetter]);
            }
            document.getElementById('cipher').value = encryptedMessage.toLowerCase();
        }
