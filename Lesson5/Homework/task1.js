/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>

*/

const resultBlock = document.createElement("div");
resultBlock.id = "CommentsFeed";
document.body.appendChild(resultBlock);

function Comment(name = "unnamed", text = "", avatarUrl = 'images/1.jpg') {
  this.name = name;
  this.text = text;
  this.avatarUrl =avatarUrl;
  this.likes=0;

  this.incLikes = function() {
    this.likes++;
    ShowComments();
  }
  }

let comment1 = new Comment("Hello", "My name's Garfild" );
let comment2 = new Comment("Hi", "How are you?" );
let comment3 = new Comment("Alohha", "What are you doing?" );
let comment4 = new Comment("Good day", "What do you do?" );

let CommentsArray = [comment1, comment2, comment3, comment4]


function ShowComments(commentsArray) {
  // this.commentsArray = commentsArray
  this.show = function() {
     // this.commentsArray.forEach(comment => {
     commentsArray.forEach(comment => {

        const newName = document.createElement("h3");
        newName.innerText = comment.name;

        resultBlock.appendChild(newName);

        const newText = document.createElement("p");
        newText.innerText = comment.text;

        resultBlock.appendChild(newText);

        const btnLike = document.createElement("button");
        btnLike.innerText = "likes: " + comment.likes;

        btnLike.addEventListener("click", evt => {
          comment.incLikes();
          btnLike.innerText = "likes: " + comment.likes;
        })
        resultBlock.appendChild(btnLike);

        const newImg = document.createElement("img");
        newImg.src = comment.avatarUrl;
        newImg.style.width = "150px";

        resultBlock.appendChild(newImg);

      })
    }
}

let displayComments = new ShowComments(CommentsArray);

displayComments.show()
